<?php

/**
 * Class Rosiek_WarehouseRules_Helper_Data
 *
 * @author      [Andrzej Rosiek <arosiek87@gmail.com>]
 */
class Rosiek_WarehouseRules_Helper_Data extends Mage_Core_Helper_Abstract
{
    const  DROP_SHIPPING_ATTRIBUTE_CODE = 'drop_shipping';
    const  WAREHOUSE_ATTRIBUTE_CODE     = 'warehouse';

    protected $dropShippingAttributeId;

    /**
     * Return drop-shipping attribute code
     *
     * @return string
     */
    public function getDropShippingAttributeCode()
    {
        return self::DROP_SHIPPING_ATTRIBUTE_CODE;
    }

    /**
     * Return warehouse attribute code
     *
     * @return string
     */
    public function getWarehouseAttributeCode()
    {
        return self::WAREHOUSE_ATTRIBUTE_CODE;
    }
}
