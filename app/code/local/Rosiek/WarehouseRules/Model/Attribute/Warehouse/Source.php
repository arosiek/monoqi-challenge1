<?php

/**
 * Class Rosiek_WarehouseRules_Model_Attribute_Warehouse_Source
 *
 * @author      [Andrzej Rosiek <arosiek87@gmail.com>]
 */
class Rosiek_WarehouseRules_Model_Attribute_Warehouse_Source extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    const NONE_LABEL = 'None';
    const NONE_VALUE = 0;

    /**
     * Get all options created in admin panel and add 'None' option before
     *
     * @param bool $withEmpty
     * @param bool $defaultValues
     *
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        $options = parent::getAllOptions(false, $defaultValues);

        if (!is_array($this->_options)) {
            $this->_options = [];
            $options        = [];
        }

        array_unshift(
            $options,
            [
                'label' => Mage::helper('rosiek_warehouserules')->__(self::NONE_LABEL),
                'value' => self::NONE_VALUE
            ]
        );

        if ($withEmpty) {
            array_unshift(
                $options,
                [
                    'label' => '',
                    'value' => ''
                ]
            );
        }

        return $options;
    }
}
