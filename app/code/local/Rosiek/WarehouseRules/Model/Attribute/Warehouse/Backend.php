<?php

use Rosiek_WarehouseRules_Model_Attribute_Warehouse_Source as WarehouseSource;
use Mage_Eav_Model_Entity_Attribute_Source_Boolean as BooleanSource;
use Rosiek_WarehouseRules_Model_Resource_Setup as SetupClass;

/**
 * Class Rosiek_WarehouseRules_Model_Attribute_Warehouse_Backend
 *
 * @author      [Andrzej Rosiek <arosiek87@gmail.com>]
 */
class Rosiek_WarehouseRules_Model_Attribute_Warehouse_Backend extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    /** @var Rosiek_WarehouseRules_Helper_Data */
    protected $helper;

    public function __construct()
    {
        $this->helper = Mage::helper('rosiek_warehouserules');
    }

    /**
     * @param Varien_Object $object
     *
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function validate($object)
    {
        $attrCode = $this->getAttribute()->getAttributeCode();
        $value    = $object->getData($attrCode);

        if (
            $value === strval(WarehouseSource::NONE_VALUE) &&
            (
                !$this->isDropShippingAttributeYes($object) ||
                !$this->isDropShippingAttributeYesForEachStore($object)
            )
        ) {
            throw Mage::exception(
                'Rosiek_WarehouseRules',
                $this->helper->__(
                    "The value of attribute '%s' cannot be '%s' when attribute '%s' is '%s' in any store or globally",
                    $this->helper->getWarehouseAttributeCode(),
                    $this->helper->__(WarehouseSource::NONE_LABEL),
                    $this->helper->getDropShippingAttributeCode(),
                    $this->helper->__('No')
                )
            );
        }

        return parent::validate($object);
    }

    /**
     * @param Varien_Object $object
     *
     * @return bool
     */
    protected function isDropShippingAttributeYes($object)
    {
        return $object->getData($this->helper->getDropShippingAttributeCode()) == BooleanSource::VALUE_YES;
    }

    /**
     * @param $object
     *
     * @return bool
     * @throws Mage_Core_Exception
     */
    protected function isDropShippingAttributeYesForEachStore($object)
    {
        /** @var Mage_Eav_Model_Resource_Attribute_Collection $attributeCollection */
        $attributeCollection = $this->getDropShippingAttributeCollectionWithValues($object->getId());
        if (
            $attributeCollection->getSize() == 0 &&
            $object->getStoreId() != Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID
        ) {
            throw Mage::exception(
                'Rosiek_WarehouseRules',
                $this->helper->__(
                    "Set '%s' value globally first",
                    $this->helper->getDropShippingAttributeCode()
                )
            );
        }

        foreach ($attributeCollection as $attributeProductVal) {
            if ($attributeProductVal->getValue() == BooleanSource::VALUE_NO) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param int $productId
     *
     * @return Mage_Eav_Model_Resource_Attribute_Collection
     */
    protected function getDropShippingAttributeCollectionWithValues($productId)
    {
        $productTable = Mage::getSingleton('core/resource')->getTableName('catalog/product');
        /** @var Mage_Eav_Model_Resource_Attribute_Collection $attributeCollection */
        $attributeCollection = Mage::getModel('eav/entity_attribute')->getCollection();
        $attributeCollection->getSelect()
            ->joinLeft(
                ['cpei' => $productTable . '_' . SetupClass::DROP_SHIPPING_ATTRIBUTE_TYPE],
                'cpei.attribute_id = main_table.attribute_id'
            );
        $attributeCollection
            ->addFieldToFilter('attribute_code', array('eq' => $this->helper->getDropShippingAttributeCode()))
            ->addFieldToFilter('cpei.entity_id', array('eq' => (int) $productId));

        $attributeCollection->addExpressionFieldToSelect(
            'attribute_id',
            'LOWER(CONCAT(main_table.attribute_id,"-",cpei.store_id))',
            [
                'main_table.attribute_id',
                'cpei.store_id'
            ]
        );

        return $attributeCollection;
    }
}
