<?php

/**
 * Class Rosiek_WarehouseRules_Model_Resource_Setup
 *
 * @author      [Andrzej Rosiek <arosiek87@gmail.com>]
 */
class Rosiek_WarehouseRules_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup
{
    const  DROP_SHIPPING_LABEL           = 'Drop-shipping';
    const  DROP_SHIPPING_ATTRIBUTE_ORDER = 100;
    const  DROP_SHIPPING_ATTRIBUTE_TYPE  = 'int';
    const  WAREHOUSE_LABEL               = 'Warehouse';
    const  WAREHOUSE_ATTRIBUTE_ORDER     = 101;
    const  WAREHOUSE_ATTRIBUTE_TYPE      = 'int';

    /**
     * @var Rosiek_WarehouseRules_Helper_Data
     */
    protected $helper;

    /**
     * Rosiek_WarehouseRules_Model_Resource_Setup constructor.
     *
     * @param string $resourceName
     */
    public function __construct($resourceName)
    {
        $this->helper = Mage::helper('rosiek_warehouserules');

        parent::__construct($resourceName);
    }

    protected function _prepareValues($attr)
    {
        $data = parent::_prepareValues($attr);
        $data = array_merge(
            $data, array(
                     'frontend_input_renderer'       => $this->_getValue($attr, 'input_renderer'),
                     'is_global'                     => $this->_getValue(
                         $attr, 'global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
                     ),
                     'is_visible'                    => $this->_getValue($attr, 'visible', 1),
                     'is_searchable'                 => $this->_getValue($attr, 'searchable', 0),
                     'is_filterable'                 => $this->_getValue($attr, 'filterable', 0),
                     'is_comparable'                 => $this->_getValue($attr, 'comparable', 0),
                     'is_visible_on_front'           => $this->_getValue($attr, 'visible_on_front', 0),
                     'is_wysiwyg_enabled'            => $this->_getValue($attr, 'wysiwyg_enabled', 0),
                     'is_html_allowed_on_front'      => $this->_getValue($attr, 'is_html_allowed_on_front', 0),
                     'is_visible_in_advanced_search' => $this->_getValue($attr, 'visible_in_advanced_search', 0),
                     'is_filterable_in_search'       => $this->_getValue($attr, 'filterable_in_search', 0),
                     'used_in_product_listing'       => $this->_getValue($attr, 'used_in_product_listing', 0),
                     'used_for_sort_by'              => $this->_getValue($attr, 'used_for_sort_by', 0),
                     'apply_to'                      => $this->_getValue($attr, 'apply_to'),
                     'position'                      => $this->_getValue($attr, 'position', 0),
                     'is_configurable'               => $this->_getValue($attr, 'is_configurable', 1),
                     'is_used_for_promo_rules'       => $this->_getValue($attr, 'used_for_promo_rules', 0)
                 )
        );

        return $data;
    }

    /**
     *  Add drop-shipping attribute
     *
     * @return Rosiek_WarehouseRules_Model_Resource_Setup
     */
    public function addDropShippingAttribute()
    {
        $this->addAttribute(
            Mage_Catalog_Model_Product::ENTITY,
            $this->helper->getDropShippingAttributeCode(),
            [
                'type'                    => self::DROP_SHIPPING_ATTRIBUTE_TYPE,
                'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                'input'                   => 'select',
                'label'                   => self::DROP_SHIPPING_LABEL,
                'source'                  => 'eav/entity_attribute_source_boolean',
                'required'                => 1,
                'user_defined'            => 1,
                'is_configurable'         => 0,
                'used_in_product_listing' => 1
            ]
        );
        $this->addAttributeToSet(
            Mage_Catalog_Model_Product::ENTITY,
            $this->_defaultAttributeSetName,
            $this->_defaultGroupName,
            $this->helper->getDropShippingAttributeCode(),
            self::DROP_SHIPPING_ATTRIBUTE_ORDER
        );

        return $this;
    }

    /**
     * Add warehouse attribute
     *
     * @return Rosiek_WarehouseRules_Model_Resource_Setup
     */
    public function addWarehouseAttribute()
    {
        $this->addAttribute(
            Mage_Catalog_Model_Product::ENTITY,
            $this->helper->getWarehouseAttributeCode(),
            [
                'type'                    => self::DROP_SHIPPING_ATTRIBUTE_TYPE,
                'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                'input'                   => 'select',
                'label'                   => self::WAREHOUSE_LABEL,
                'backend'                 => 'rosiek_warehouserules/attribute_warehouse_backend',
                'source'                  => 'rosiek_warehouserules/attribute_warehouse_source',
                'required'                => 1,
                'user_defined'            => 1,
                'is_configurable'         => 0,
                'used_in_product_listing' => 1,
                'option'                  => [
                    'values' => [
                        'Germany',
                        'Netherlands'
                    ]
                ]
            ]
        );
        $this->addAttributeToSet(
            Mage_Catalog_Model_Product::ENTITY,
            $this->_defaultAttributeSetName,
            $this->_defaultGroupName,
            $this->helper->getWarehouseAttributeCode(),
            self::WAREHOUSE_ATTRIBUTE_ORDER
        );

        return $this;
    }
}
