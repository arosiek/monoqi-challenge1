<?php

use Mage_Eav_Model_Entity_Attribute_Source_Boolean as BooleanSource;
use Rosiek_WarehouseRules_Model_Attribute_Warehouse_Source as WarehouseSource;

/**
 * Class Rosiek_WarehouseRules_Model_Observer_AddRuleToProductViewObserver
 *
 * @author      [Andrzej Rosiek <arosiek87@gmail.com>]
 */
class Rosiek_WarehouseRules_Model_Observer_AddRuleToProductViewObserver
{
    /**
     * Check Warehouse/Drop-Shipping Rules and hide product or do nothing
     *
     * @param Varien_Event_Observer $observer
     */
    public function checkWarehouseRulesAndMakeActions(Varien_Event_Observer $observer)
    {
        /** @var Rosiek_WarehouseRules_Helper_Data $helper */
        $helper = Mage::helper('rosiek_warehouserules');

        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getProduct();

        $dropShipping = $product->getData($helper->getDropShippingAttributeCode());
        $warehouse    = $product->getData($helper->getWarehouseAttributeCode());
        if (
            $dropShipping == BooleanSource::VALUE_YES ||
            ($dropShipping == BooleanSource::VALUE_NO && $warehouse != WarehouseSource::NONE_VALUE)
        ) {
            //do nothing
            return;
        }

        //hide product
        $product->setVisibility(1);
    }
}
