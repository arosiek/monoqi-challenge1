<?php

use Mage_Eav_Model_Entity_Attribute_Source_Boolean as BooleanSource;
use Rosiek_WarehouseRules_Model_Attribute_Warehouse_Source as WarehouseSource;

/**
 * Class Rosiek_WarehouseRules_Model_Observer_AddRuleToCollectionObserver
 *
 * @author      [Andrzej Rosiek <arosiek87@gmail.com>]
 */
class Rosiek_WarehouseRules_Model_Observer_AddRuleToCollectionObserver
{
    /**
     * @var Rosiek_WarehouseRules_Helper_Data
     */
    protected $helper;

    public function __construct()
    {
        $this->helper = Mage::helper('rosiek_warehouserules');
    }

    /**
     * Add Warehouse/Drop-Shipping Rules to product collection where part
     *
     * @param Varien_Event_Observer $observer
     */
    public function addWarehouseRulesToCollection(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $productCollection */
        $productCollection = $observer->getCollection();

        if ($productCollection->isEnabledFlat()) {
            $this->addFiltersForFlat($productCollection);
        } else {
            $this->addFiltersForEav($productCollection);
        }

        $productCollection
            ->addBindParam('drop_ship_false', BooleanSource::VALUE_NO)
            ->addBindParam('drop_ship_true', BooleanSource::VALUE_YES)
            ->addBindParam('warehouse_none', WarehouseSource::NONE_VALUE);
    }

    /**
     * Add Warehouse/Drop-Shipping Rules to flat product collection where part
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $productCollection
     */
    protected function addFiltersForFlat(Mage_Catalog_Model_Resource_Product_Collection $productCollection)
    {
        $dsAttr = $this->helper->getDropShippingAttributeCode();
        $wAttr  = $this->helper->getWarehouseAttributeCode();
        $productCollection->getSelect()
            ->where(
                new Zend_Db_Expr(
                    "{$dsAttr}=:drop_ship_true " .
                    "(" .
                    "{$dsAttr}=:drop_ship_false AND " .
                    "{$wAttr}!=:warehouse_none" .
                    ")"
                )
            );
    }

    /**
     * Join eav attribute tables and add Warehouse/Drop-Shipping Rules to eav product collection where part
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $productCollection
     */
    protected function addFiltersForEav(Mage_Catalog_Model_Resource_Product_Collection $productCollection)
    {
        $this->joinAttributeTables($productCollection);
        $dsAttr = $this->helper->getDropShippingAttributeCode();
        $wAttr  = $this->helper->getWarehouseAttributeCode();
        $productCollection->getSelect()
            ->where(
                new Zend_Db_Expr(
                    "IF(at_{$dsAttr}.value_id > 0, at_{$dsAttr}.value, at_{$dsAttr}_default.value)=:drop_ship_true OR " .
                    "(" .
                    "IF(at_{$dsAttr}.value_id > 0, at_{$dsAttr}.value, at_{$dsAttr}_default.value)=:drop_ship_false AND " .
                    "`at_{$wAttr}`.`value`!=:warehouse_none" .
                    ")"
                )
            );
    }

    /**
     * Join eav attribute tables for Warehouse/Drop-Shipping attributes
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $productCollection
     */
    public function joinAttributeTables(Mage_Catalog_Model_Resource_Product_Collection $productCollection)
    {
        $productCollection
            ->joinAttribute(
                $this->helper->getDropShippingAttributeCode(),
                Mage_Catalog_Model_Product::ENTITY . '/' . $this->helper->getDropShippingAttributeCode(),
                'entity_id'
            )
            ->joinAttribute(
                $this->helper->getWarehouseAttributeCode(),
                Mage_Catalog_Model_Product::ENTITY . '/' . $this->helper->getWarehouseAttributeCode(),
                'entity_id'
            );
    }
}
