<?php

/* @var $installer Rosiek_WarehouseRules_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer
    ->addDropShippingAttribute()
    ->addWarehouseAttribute();

$installer->endSetup();
