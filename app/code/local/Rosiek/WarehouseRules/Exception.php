<?php

/**
 * Class Rosiek_WarehouseRules_Exception
 *
 * @author      [Andrzej Rosiek <arosiek87@gmail.com>]
 */
class Rosiek_WarehouseRules_Exception extends Mage_Core_Exception
{
}
